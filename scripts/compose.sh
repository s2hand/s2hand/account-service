#!/bin/sh

ACTION=${1:-"up"}
ENV_TAG=""
ENV_FILE=""
ENV_COMPOSE=""

if [ "$2" == "dev" ]
then
  ENV_TAG="dev"
  ENV_FILE=".env.development"
else
  ENV_TAG="prod"
  ENV_FILE=".env.production"
fi

if [ "$ACTION" == "down" ]
then
  docker-compose --env-file=./environments/$ENV_FILE down
else
  export DEPLOY_ENV=$ENV_TAG
  docker-compose -f docker-compose.yml \
   --env-file=./environments/$ENV_FILE up --build -d
fi
